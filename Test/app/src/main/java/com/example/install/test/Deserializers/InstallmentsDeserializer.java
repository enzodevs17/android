package com.example.install.test.Deserializers;
import com.example.install.test.Models.InstallmentsModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class InstallmentsDeserializer implements JsonDeserializer<InstallmentsModel> {

    @Override
    public InstallmentsModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        String cardName = json.getAsJsonObject().get("payment_method_id").getAsString();
        String bankName = json.getAsJsonObject().get("issuer").getAsJsonObject().get("name").getAsString();
        String bankImage = json.getAsJsonObject().get("issuer").getAsJsonObject().get("thumbnail").getAsString();

        ArrayList<String>  paymentPlan = new ArrayList<String>();
        JsonArray array = json.getAsJsonObject().get("payer_costs").getAsJsonArray();

        for(JsonElement share : array){
            paymentPlan.add(share.getAsJsonObject().get("recommended_message").getAsString());
        }
        return  new InstallmentsModel(cardName,bankName,bankImage,paymentPlan);
    }
}
