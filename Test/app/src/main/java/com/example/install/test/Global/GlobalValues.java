package com.example.install.test.Global;

import android.app.Application;

public class GlobalValues extends Application {

    //region .: Properties :.
    private double amountValue;
    private String bankName;
    private String cardName;
    private String payment;
    private String image;
    //endregion

    //region .: Get And Set :.

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public double getAmountValue(){
        return amountValue;
    }

    public void setAmountValue(double amountValue){
        this.amountValue = amountValue;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }
    //endregion

}
