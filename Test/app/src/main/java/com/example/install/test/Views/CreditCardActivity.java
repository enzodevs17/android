package com.example.install.test.Views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.example.install.test.API.API;
import com.example.install.test.API.APIServices.PaymentService;
import com.example.install.test.Adapter.CreditCardAdapter;
import com.example.install.test.Models.CreditCardModel;
import com.example.install.test.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreditCardActivity extends AppCompatActivity {

    //region .: Properties :.
    private ProgressDialog progress;
    private RecyclerView recyclerView;
    //endregion

    //region .: Overrible :.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);
        getSupportActionBar().setTitle(R.string.credit_card_activity_title);

        //region .: RecyclerView Configuration :.
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewCreditCard);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);
        //endregion

        this.showProgressDialog();

        PaymentService service = API.getApi().create(PaymentService.class);

        Call<List<CreditCardModel>> creditCardListCall = service.getCreditCardList(API.PUBLIC_KEY);

        creditCardListCall.enqueue(new Callback<List<CreditCardModel>>() {

            @Override
            public void onResponse(Call<List<CreditCardModel>> call, Response<List<CreditCardModel>> response) {
                enableCreditCardList(response.body());
                closeProgressDialog();
            }

            @Override
            public void onFailure(Call<List<CreditCardModel>> call, Throwable t) {
                closeProgressDialog();
                Toast.makeText(CreditCardActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        });
    }
    //endregion

    //region .: Progress Dialog :.
    private void showProgressDialog() {
        this.progress = new ProgressDialog(this);
        this.progress.setTitle(R.string.loading_title);
        this.progress.setMessage("Espere mientras carga...");
        this.progress.setCancelable(false);
        this.progress.show();
    }

    private void closeProgressDialog() {
        this.progress.dismiss();
    }
    //endregion

    //region .: Application Flow :.

    private void enableCreditCardList(final List<CreditCardModel> lstCreditCards) {
        CreditCardAdapter adapter = new CreditCardAdapter(this, lstCreditCards);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String creditCardId = lstCreditCards.get(recyclerView.getChildAdapterPosition(view)).getId();
                if(!TextUtils.isEmpty(creditCardId)){
                    goToBankView(creditCardId);
                }else{
                    Toast.makeText(CreditCardActivity.this, R.string.credit_card_activity_invalid, Toast.LENGTH_LONG).show();
                }
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void goToBankView(String creditCardId) {
        Intent intent = new Intent(this, BankActivity.class);
        intent.putExtra("credit_card",creditCardId);
        startActivity(intent);
    }

    //endregion
}
