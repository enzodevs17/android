package com.example.install.test.Models;
import java.util.ArrayList;

public class InstallmentsModel {

    //region .: Properties :.
    public String cardName;
    public String bankName;
    public String bankImage;
    public ArrayList<String> paymentPlan;
    //endregion

    //region .: Constructor :.
    public InstallmentsModel(String cardName, String bankName, String bankImage, ArrayList<String> paymentPlan) {
        this.cardName = cardName;
        this.bankName = bankName;
        this.bankImage = bankImage;
        this.paymentPlan = paymentPlan;
    }
    //endregion

    //region .: Get and Set :.
    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankImage() {
        return bankImage;
    }

    public void setBankImage(String bankImage) {
        this.bankImage = bankImage;
    }

    public ArrayList<String> getPaymentPlan() {
        return paymentPlan;
    }

    public void setPaymentPlan(ArrayList<String> paymentPlan) {
        this.paymentPlan = paymentPlan;
    }
    //endregion
}
