package com.example.install.test.Views;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.install.test.Global.GlobalValues;
import com.example.install.test.R;

public class MainActivity extends AppCompatActivity {

    //region .: Properties :.
    private  EditText editText;
    //endregion

    //region .: Override :.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle(R.string.main_activity_title);

        editText = (EditText)findViewById(R.id.input_numeral);
        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            Boolean enable = bundle.getBoolean("enable_alert");
            if(enable){
                this.onAlertShow();
            }
        }
    }

    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }
    //endregion

    //region .: OnClick :.
    public void onPayment(View view){
        this.goToCreditCardView();
    }
    //endregion

    //region .: Application Flow :.

    private void goToCreditCardView() {

        if (!TextUtils.isEmpty(editText.getText())) {

            ((GlobalValues) getApplicationContext()).setAmountValue(Double.parseDouble(editText.getText().toString()));
            Intent intent = new Intent(this, CreditCardActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(MainActivity.this, R.string.main_activity_input_empty, Toast.LENGTH_LONG).show();
        }
    }

   //region .: AlertShow Configuration :.
    private void onAlertShow(){

        AlertDialog.Builder customDialog = new AlertDialog.Builder(MainActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.alert_info,null);
        customDialog.setView(mView);
        final AlertDialog alert = customDialog.create();

        TextView bankName = (TextView) mView.findViewById(R.id.textViewAlertBank);
        bankName.setText(((GlobalValues) this.getApplication()).getBankName());

        TextView cardName = (TextView) mView.findViewById(R.id.textViewAlertCreditCardName);
        cardName.setText(((GlobalValues) this.getApplication()).getCardName());

        TextView amountName = (TextView) mView.findViewById(R.id.textViewAlertAmount);
        amountName.setText("$ "+ String.valueOf(((GlobalValues) this.getApplication()).getAmountValue()));

        TextView paymentName = (TextView) mView.findViewById(R.id.textViewAlertPayment);
        paymentName.setText(((GlobalValues) this.getApplication()).getPayment());

        ImageView image = (ImageView) mView.findViewById(R.id.imageViewAlertBank);
        Glide.with(this).load(((GlobalValues) this.getApplication()).getImage()).into(image);

        Button btnClose = (Button) mView.findViewById(R.id.buttonAlertClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert.show();
    }
    //endregion

    //endregion
}
