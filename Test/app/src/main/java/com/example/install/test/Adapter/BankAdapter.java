package com.example.install.test.Adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.install.test.Models.BankModel;
import com.example.install.test.R;
import java.util.List;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.ViewHolder> implements View.OnClickListener {

    //region .: Properties :.
    private Context context;
    private List<BankModel> lstBanks;
    private View.OnClickListener listener;
    //endregion

    //region .: Constructor :.
    public BankAdapter(Context context, List<BankModel> lstBanks){
        this.context = context;
        this.lstBanks = lstBanks;
    }
    //endregion

    //region .: Viewholder :.
    @Override
    public BankAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.universal_item,parent,false);
        view.setOnClickListener(this);
        return new BankAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BankAdapter.ViewHolder holder, int position) {
        holder.bankName.setText(lstBanks.get(position).getBankName());
        Glide.with(context).load(lstBanks.get(position).getBankImage()).into(holder.bankImage);
    }

    @Override
    public int getItemCount() {
        return lstBanks.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView bankImage;
        public TextView bankName;

        public ViewHolder(View itemView){
            super(itemView);
            this.bankImage = (ImageView) itemView.findViewById(R.id.imageViewItem);
            this.bankName = (TextView) itemView.findViewById(R.id.textViewItem);
        }
    }

    //endregion

    //region .: Click Item :.
    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null){
            listener.onClick(view);
        }
    }
    //endregion
}
