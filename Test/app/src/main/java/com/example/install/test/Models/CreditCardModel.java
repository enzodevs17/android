package com.example.install.test.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CreditCardModel {

    //region .: Properties :.
    @SerializedName("name")
    private String cardName;
    @SerializedName("thumbnail")
    private String cardImage;
    private String id;
    //endregion

    //region .: Constructor :.
    public CreditCardModel() {}

    public CreditCardModel(String cardName, String image, String id) {
        this.cardName = cardName;
        this.cardImage = image;
        this.id = id;
    }

    //endregion

    //region .: Get and Set :.

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardImage() {
        return cardImage;
    }

    public void setCardImage(String cardImage) {
        this.cardImage = cardImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //endregion
}
