package com.example.install.test.API.APIServices;
import com.example.install.test.Models.BankModel;
import com.example.install.test.Models.CreditCardModel;
import com.example.install.test.Models.InstallmentsModel;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PaymentService {

    @GET("payment_methods")
    Call<List<CreditCardModel>> getCreditCardList(@Query("public_key") String public_key);

    @GET("payment_methods/card_issuers")
    Call<List<BankModel>> getBankList(@Query("public_key") String public_key, @Query("payment_method_id") String payment_method);

    @GET("payment_methods/installments")
    Call<List<InstallmentsModel>> getInstallments (@Query("public_key") String public_key, @Query("amount") double amount,
                                             @Query("payment_method_id") String payment_method, @Query("issuer.id") Number issuerId);
}