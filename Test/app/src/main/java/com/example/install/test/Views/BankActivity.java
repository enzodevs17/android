package com.example.install.test.Views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.example.install.test.API.API;
import com.example.install.test.API.APIServices.PaymentService;
import com.example.install.test.Adapter.BankAdapter;
import com.example.install.test.Adapter.CreditCardAdapter;
import com.example.install.test.Models.BankModel;
import com.example.install.test.Models.CreditCardModel;
import com.example.install.test.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankActivity extends AppCompatActivity {

    //region .: Properties :.
    private ProgressDialog progress;
    private RecyclerView recyclerView;
    private String creditCard;
    //endregion

    //region .: Overrible :.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank);
        getSupportActionBar().setTitle(R.string.bank_activity_title);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){

            creditCard = bundle.getString("credit_card");

            //region .: RecyclerView Configuration :.
            recyclerView = (RecyclerView) findViewById(R.id.recyclerViewBank);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setLayoutManager(layoutManager);
            //endregion

            this.showProgressDialog();

            PaymentService service = API.getApi().create(PaymentService.class);

            Call<List<BankModel>> bankListCall = service.getBankList(API.PUBLIC_KEY, creditCard);
            bankListCall.enqueue(new Callback<List<BankModel>>() {
                @Override
                public void onResponse(Call<List<BankModel>> call, Response<List<BankModel>> response) {
                    if(response.body().size() > 0){
                        enableBankList(response.body());
                        closeProgressDialog();
                    }else{
                        closeProgressDialog();
                        Toast.makeText(BankActivity.this, R.string.bank_activity_credit_card_error, Toast.LENGTH_LONG).show();
                        onBackPressed();
                    }

                }

                @Override
                public void onFailure(Call<List<BankModel>> call, Throwable t) {
                    closeProgressDialog();
                    Toast.makeText(BankActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            });
        }
    }
    //endregion

    //region .: Progress Dialog :.
    private void showProgressDialog() {
        this.progress = new ProgressDialog(this);
        this.progress.setTitle(R.string.loading_title);
        this.progress.setMessage("Espere mientras carga...");
        this.progress.setCancelable(false);
        this.progress.show();
    }

    private void closeProgressDialog() {
        this.progress.dismiss();
    }
    //endregion

    //region .: Application Flow :.
    private void enableBankList(final List<BankModel> lstBanks) {
        BankAdapter adapter = new BankAdapter(this, lstBanks);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String bankId = lstBanks.get(recyclerView.getChildAdapterPosition(view)).getId();

                if (!TextUtils.isEmpty(bankId)) {
                    goToInstallmentsView(bankId);
                } else {
                    Toast.makeText(BankActivity.this, R.string.bank_activity_payment_plan_invalid, Toast.LENGTH_LONG).show();
                }
            }
        });
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    private void goToInstallmentsView(String bankId) {
        Intent intent = new Intent(this, InstallmentsActivity.class);
        intent.putExtra("credit_card",creditCard);
        intent.putExtra("bankId",bankId);
        startActivity(intent);
    }

    //endregion
}
