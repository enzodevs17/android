package com.example.install.test.Models;

import com.google.gson.annotations.SerializedName;

public class BankModel {

    //region .: Properties :.
    @SerializedName("name")
    private String bankName;
    @SerializedName("thumbnail")
    private String bankImage;
    private String id;
    //endregion

    //region .: Constructor :.
    public BankModel(){}

    public BankModel(String bankName, String bankImage, String id) {
        this.bankName = bankName;
        this.bankImage = bankImage;
        this.id = id;
    }
    //endregion

    //region .: Get and Set :.

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankImage() {
        return bankImage;
    }

    public void setBankImage(String bankImage) {
        this.bankImage = bankImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //endregion

}
