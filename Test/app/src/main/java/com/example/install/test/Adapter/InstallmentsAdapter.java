package com.example.install.test.Adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import com.example.install.test.R;
import java.util.List;

public class InstallmentsAdapter extends RecyclerView.Adapter<InstallmentsAdapter.ViewHolder> {

    //region .: Properties :.
    private Context context;
    private List<String> lstPaymentOptions;
    private int lastSelectedPosition = -1;
    //endregion

    //region .: Get :.
    public int getLastSelectedPosition() {
        return lastSelectedPosition;
    }
    //endregion

    //region .: Constructor :.
    public InstallmentsAdapter(Context context, List<String> lstPaymentOptions){
        this.context = context;
        this.lstPaymentOptions = lstPaymentOptions;
    }
    //endregion

    //region .: Viewholder :.
    @Override
    public InstallmentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_item,parent,false);
        InstallmentsAdapter.ViewHolder viewHolder = new InstallmentsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final InstallmentsAdapter.ViewHolder holder, int position) {
        holder.textViewPayment.setText(lstPaymentOptions.get(position));
        holder.radioButtonPayment.setChecked(lastSelectedPosition == position);
    }

    @Override
    public int getItemCount() {
        return lstPaymentOptions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textViewPayment;
        public RadioButton radioButtonPayment;

        public ViewHolder(View view) {
            super(view);

            textViewPayment = (TextView) view.findViewById(R.id.textViewPayment);
            radioButtonPayment = (RadioButton) view.findViewById(R.id.radioButtonPayment);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });

            radioButtonPayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }

    //endregion
}

