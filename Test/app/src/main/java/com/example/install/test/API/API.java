package com.example.install.test.API;

import com.example.install.test.Deserializers.InstallmentsDeserializer;
import com.example.install.test.Models.InstallmentsModel;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {
    public static final String BASE_URL = "https://api.mercadopago.com/v1/";
    public static final String PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";
    private static Retrofit retrofit = null;

    public static Retrofit getApi(){
        if(retrofit == null){

            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(InstallmentsModel.class, new InstallmentsDeserializer());

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(builder.create()))
                    .build();
        }
        return retrofit;
    }
}
