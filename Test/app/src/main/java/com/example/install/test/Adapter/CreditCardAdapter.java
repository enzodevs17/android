package com.example.install.test.Adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.install.test.Models.CreditCardModel;
import com.example.install.test.R;
import java.util.List;

public class CreditCardAdapter extends RecyclerView.Adapter<CreditCardAdapter.ViewHolder> implements View.OnClickListener  {

    //region .: Properties :.
    private Context context;
    private List<CreditCardModel> lstCreditCards;
    private View.OnClickListener listener;
    //endregion

    //region .: Constructor :.
    public CreditCardAdapter(Context context, List<CreditCardModel> lstCreditCards){
        this.context = context;
        this.lstCreditCards = lstCreditCards;
    }
    //endregion

    //region .: Viewholder :.
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.universal_item,parent,false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.cardName.setText(lstCreditCards.get(position).getCardName());
        Glide.with(context).load(lstCreditCards.get(position).getCardImage()).into(holder.cardImage);
    }

    @Override
    public int getItemCount() {
        return lstCreditCards.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView cardImage;
        public TextView cardName;

        public ViewHolder(View itemView){
            super(itemView);
            this.cardImage = (ImageView) itemView.findViewById(R.id.imageViewItem);
            this.cardName = (TextView) itemView.findViewById(R.id.textViewItem);
        }
    }

    //endregion

    //region .: Click Item :.
    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null){
            listener.onClick(view);
        }
    }
    //endregion

}
