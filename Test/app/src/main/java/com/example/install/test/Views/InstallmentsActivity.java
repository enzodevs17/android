package com.example.install.test.Views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.example.install.test.API.API;
import com.example.install.test.API.APIServices.PaymentService;
import com.example.install.test.Adapter.CreditCardAdapter;
import com.example.install.test.Adapter.InstallmentsAdapter;
import com.example.install.test.Global.GlobalValues;
import com.example.install.test.Models.CreditCardModel;
import com.example.install.test.Models.InstallmentsModel;
import com.example.install.test.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Boolean.TRUE;

public class InstallmentsActivity extends AppCompatActivity {

    //region .: Properties :.
    private RecyclerView recyclerView;
    private InstallmentsAdapter adapter;
    private InstallmentsModel operationItem;
    private ProgressDialog progress;
    //endregion

    //region .: Overrible :.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installments);
        getSupportActionBar().setTitle(R.string.installments_activity_title);

        //region .: RecyclerView Configuration :.
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewInstallments);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);
        //endregion

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){

            String creditCard = bundle.getString("credit_card");
            Number bank = Integer.parseInt(bundle.getString("bankId"));
            double amount = ((GlobalValues) this.getApplication()).getAmountValue();

            this.showProgressDialog();

            PaymentService service = API.getApi().create(PaymentService.class);

            Call<List<InstallmentsModel>> installmentsCall = service.getInstallments(API.PUBLIC_KEY, amount, creditCard, bank);
            installmentsCall.enqueue(new Callback<List<InstallmentsModel>>() {
                @Override
                public void onResponse(Call<List<InstallmentsModel>> call, Response<List<InstallmentsModel>> response) {
                    if(response.body() != null && response.body().size() > 0){
                        closeProgressDialog();
                        operationItem =  response.body().get(0);
                        enablePaymentList();
                    }else{
                        closeProgressDialog();
                        Toast.makeText(InstallmentsActivity.this, R.string.bank_activity_payment_plan_invalid, Toast.LENGTH_LONG).show();
                        onBackPressed();
                    }

                }

                @Override
                public void onFailure(Call<List<InstallmentsModel>> call, Throwable t) {
                    closeProgressDialog();
                    Toast.makeText(InstallmentsActivity.this, R.string.server_error, Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            });
        }
    }
    //endregion

    //region .: Application Flow :.
    private void enablePaymentList() {
        adapter = new InstallmentsAdapter(this, operationItem.paymentPlan);
        recyclerView.setAdapter(adapter);
    }

    private void saveOperationValues(int result){

        ((GlobalValues) getApplicationContext()).setBankName(operationItem.bankName);
        ((GlobalValues) getApplicationContext()).setImage(operationItem.bankImage);
        ((GlobalValues) getApplicationContext()).setCardName(operationItem.cardName);
        ((GlobalValues) getApplicationContext()).setPayment(operationItem.paymentPlan.get(result));

        Intent intent = new Intent(getApplicationContext(),MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("enable_alert",TRUE);
        startActivity(intent);
    }
    //endregion

    //region .: OnClick :.
    public void onEndOperation(View view){
        if(adapter.getLastSelectedPosition() > -1){
            this.saveOperationValues(adapter.getLastSelectedPosition());
        }else {
            Toast.makeText(InstallmentsActivity.this,R.string.installments_activity_finish_operation, Toast.LENGTH_LONG).show();
        }
    }
    //endregion

    //region .: Progress Dialog :.
    private void showProgressDialog() {
        this.progress = new ProgressDialog(this);
        this.progress.setTitle(R.string.loading_title);
        this.progress.setMessage("Espere mientras carga...");
        this.progress.setCancelable(false);
        this.progress.show();
    }

    private void closeProgressDialog() {
        this.progress.dismiss();
    }
    //endregion
}
